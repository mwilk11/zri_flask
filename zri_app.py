
import flask 
from flask import render_template, request
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import io
from base64 import b64encode


#import ZRI index data csv file, fitted values based on model and residuals
zri_data = pd.read_csv('data/kdw_residuals.csv')
#change the date column to a datetime object
zri_data.loc[:,'date'] =  pd.to_datetime(zri_data.loc[:,'date'])


#function that takes a zipcode param input, checks whether it exists in the data
#if zipcode does not exist in the data, return an error 
#if zip in data make a plot of param variable inputs 
def read_csv_by_zip(zip_code_, title, variable1, variable2=None):
    zip_code = int(zip_code_)
    if zri_data[zri_data['RegionName']==zip_code].shape[0]==0:
        return 'Error'
    else:
        plt.switch_backend('agg')
        plt.plot(zri_data['date'].where(zri_data['RegionName']==zip_code),zri_data[variable1].where(zri_data['RegionName']==zip_code),label=variable1)

        if variable2 is None:
            pass
        else:
            plt.plot(zri_data['date'].where(zri_data['RegionName']==zip_code),zri_data[variable2].where(zri_data['RegionName']==zip_code),label=variable2)

        plt.title(title)
        plt.legend(loc='upper left')
        plt.axvline(dt.datetime(2018, 1, 1), color='gray')
        plt.axvline(dt.datetime(2019, 1, 1), color='gray')
        plt.xticks(rotation=20)
        #save figures into a bytes object to render it on frontend 
        bytes_image = io.BytesIO()
        plt.savefig(bytes_image, format='png')
        bytes_image.seek(0)
        plot_data = b64encode(bytes_image.getvalue()).decode('ascii')
        return plot_data


app = flask.Flask(__name__)


#initial page with html form with regex validation input for a 5 digit number
@app.route('/', methods=['GET'])
def zip_input_form():
    return render_template('form.html')


#second page which returns the graphs of the fitted model as well as a graph of residuals
#if zipcode typed in is not within the data, the user will be directed to an error page 
@app.route('/handle_data', methods=['GET','POST'])
def handle_data():
    zip_code = request.form['zip']
    zri_predictions = read_csv_by_zip(zip_code, 'ZRI Model Predictions', 'Predictions', 'Actuals')
    zri_residuals = read_csv_by_zip(zip_code, 'Residuals', 'Residuals')

    if zri_predictions =='Error' or zri_residuals =='Error':
        return render_template('index2.html', name='zipcode not found in ZRI data')
    else:
        place_name = zri_data['Area_Name'][zri_data['RegionName']==int(zip_code)].values[0]
        zip_title = 'ZRI Predictions for Zipcode: {} in {}'.format(zip_code, place_name)
        return render_template('index.html', name = zip_title, image1=zri_predictions, image2=zri_residuals)


if __name__ == '__main__':
    app.run(debug=False, port=5001)

